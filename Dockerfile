FROM debian:bookworm-slim

RUN apt-get update && \
    apt-get install -y bc bison build-essential device-tree-compiler \
	flex gcc-aarch64-linux-gnu gcc-arm-linux-gnueabi git gnutls-dev \
	libssl-dev python3-dev python3-pyelftools python3-setuptools swig \
	util-linux uuid-dev wget && \
  dpkg --add-architecture arm64 && \
  apt-get update && \
  apt-get install -y arm-trusted-firmware:arm64
